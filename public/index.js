
var n = 0;
const phi = 90;

var radius = 100;
var angleArray = [0, 1];
var xorigin = 0;
var yorigin = 0;

var startcol,endcol;
var stepcol = false;
var stepcolNum = 6;
var stepcolList = [];

startcol = [Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255)];
endcol = [Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255)];
  
let h = 125;
let s = 125;
let b = 125;
let bg = [0, 0, 10];
let bg_LIGHT = [0, 0, 240];

function setup() {

  createCanvas(windowWidth, windowHeight);
  background(0,0,0);
  rectMode(CENTER);
  translate(width / 2, height / 2);
  xorigin = width / 2;
  yorigin = height / 2;
  colorMode(HSB);
  strokeWeight(3);

  noLoop();
  
}

function keyPressed() {

  if (key =="1") {
    colorMode(RGB);
    stepcol = false;
    startcol = [220, 220, 220];
    endcol = [0, 0, 0];
  } else if (key == "2") {
    stepcol = false;
    colorMode(HSB);
    startcol =[0, 120, 120];
    endcol =[255, 120, 120];
  } else if (key == "3") {
    stepcol = false;
    colorMode(RGB);
    startcol =[Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255)];
    endcol =[Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255)];
  } else if (key == "4") {
    colorMode(HSB);
    stepcol = false;
    startcol = [Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255)];
    endcol = [Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255)];
  } else if (key == "5") {
    colorMode(HSB);
    stepcol = true;
    stepcolList = [];
    for (let i  = 0; i < stepcolNum; i++) {
      stepcolList.push([Math.floor(Math.random() * 255), Math.floor(Math.random() * 255), Math.floor(Math.random() * 255)]);
    }

  } else if (key == "w") {
    radius *= 2;
  } else if (key == "s") {
    radius/= 2;

  } else if (keyCode === RIGHT_ARROW) {
    xorigin -= 100;
  } else if (keyCode === LEFT_ARROW) {
    xorigin += 100;
  } else if (keyCode === UP_ARROW) {
    yorigin += 100;
  } else if (keyCode === DOWN_ARROW) {
    yorigin -= 100;

  } else if (keyCode == 32 && n != 15){
    n++;
    angleArray = [0,1];
    
    for (let i = 0; i < n; i++) {
      var newSteps = [...angleArray];
      newSteps.reverse();
      newSteps.pop();
      for (let j = 0; j < newSteps.length; j++) {
        newSteps[j] *= -1;
      }
      let unfold = [-1];
      angleArray = [].concat(angleArray, unfold, newSteps);
    }  
  } else if (keyCode == 8 && n != 1) {
    n--;
    angleArray = [0, 1];

    for (let i = 0; i < n-1; i++) {
      var newSteps = [...angleArray];
      newSteps.reverse();
      newSteps.pop();
      for (let j = 0; j < newSteps.length; j++) {
        newSteps[j] *= -1;
      }
      let unfold = [-1];
      angleArray = [].concat(angleArray, unfold, newSteps);
    }
  }


  redraw();


}

function draw(){
    
  clear();
  background(0, 0, 0);
  angleMode(DEGREES);
  let angle = 0;
  
  let xNow = xorigin;
  let yNow = yorigin;
  let xNew = 0;
  let yNew = 0;
  
  for (let i = 0; i < angleArray.length; i++) {
    angle += angleArray[i];
    xNew = xNow + cos(angle * phi) * radius;
    yNew = yNow + sin(angle * phi) * radius;

    if (!stepcol) {

      h = ((angleArray.length - i) * startcol[0] + i * endcol[0]) / (angleArray.length);
      s = ((angleArray.length - i) * startcol[1] + i * endcol[1]) / (angleArray.length);
      b = ((angleArray.length - i) * startcol[2] + i * endcol[2]) / (angleArray.length);
      
    } else {
      
      let colNum = (Math.floor(Math.log(i + 1) / Math.log(2)))%(stepcolNum - 1);
      h = stepcolList[colNum][0];
      s = stepcolList[colNum][1];
      b = stepcolList[colNum][2];
      
    }
    
    stroke(h,s,b);
    line(xNow, yNow, xNew, yNew);
    xNow = xNew;
    yNow = yNew;
  }

}  


function windowResized() {

  resizeCanvas(windowWidth, windowHeight);
  redraw();

}
